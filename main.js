window.onload = () => {
    const ID = new URLSearchParams(window.location.search).get('random');

    if(ID > 0) {
        let background_winner = document.querySelector(`#random-${ID}`);

        background_winner.style.backgroundColor = "#007BFF";
        
        $( "#name_random" ).fadeOut( "slow", () => {
            $( "#name_random" ).fadeIn( "slow", () => {});
        });
    }
}