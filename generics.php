<?php 

function getData() {
    $data = [
        "José",
        "Soufiane",
        "Nicolas",
        "Stéphane",
        "Cyril",
        "Manelle",
        "Celine",
        "Makan",
        "Thierry",
        "Ssonyan",
        "David",
        "Oscar",
        "Kilian",
        "Pedram",
        "Sybille"
    ];
    return $data;
}

function getRandomStudent() {
    return random_int(1, 15);
}
