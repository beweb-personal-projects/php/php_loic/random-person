<?php include('generics.php'); ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="main.js"></script>
        <title>Random Person</title>
    </head>
    <body style="background-color: #007BFF">
        <div class="container mt-1">
            <div class="row">
                <div class="col-6 bg-dark">
                    <h1 class="text-light m-2 border border-0 border-bottom border-light">Students</h1>
                    <div class="m-2">
                        <table class="table">
                            <thead>
                                <tr class="text-light">
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $students_data = getData();
                                    
                                    foreach ($students_data as $index => $value):?>
                                        <tr class="text-light" id="random-<?=$index + 1?>">
                                            <th scope="row"><?=$index + 1?></th>
                                            <td><?=$value?></td>
                                        </tr>
                                    <?endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-6 bg-light">
                    <h1 class="text-dark m-2 border border-0 border-bottom border-dark">Random Student</h1>
                    <div class="m-2 d-flex align-items-center justify-content-center" style="height: 85%;">
                        <div>
                            <?php 
                                if(isset($_GET['random'])) {
                                    echo "<img class='mb-5' src='https://media3.giphy.com/media/HcN1K7rHStA2JQtX5J/giphy.gif?cid=790b7611eabd2665d6ff6c38782bfbcd5069fd639f8aa2e6&rid=giphy.gif&ct=g' alt='You are the Choosen one !'>";
                                    // echo "<h3 class='mb-1 text-center'>You are the Choosen one !</h3>";
                                    echo "<h1 class='mb-5 text-center' id='name_random'>" . $students_data[$_GET['random'] - 1] . "</h1>";
                                    echo "<h4 class='mb-5 text-center'><ins>Classe Inversée: Sql Injection</ins></h4>";
                                } else {
                                    echo "<h4 class='mb-5'>No Random Student Yet!!</h4>";
                                }
                                
                            ?>
                            <form class="d-flex justify-content-center" method="GET" action="<?php $_PHP_SELF ?>">
                                <input name="random" value="<?=getRandomStudent()?>" hidden>
                                <input type="submit" value="Click to Randomize a Student!" class="btn btn-primary">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>